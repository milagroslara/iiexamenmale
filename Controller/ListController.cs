﻿using _2Examemale.Model;
using _2Examemale.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace _2Examemale.Controller
{
    class ListController
    {
        object Lwindow;
        SaveFileDialog sfdialog;
        OpenFileDialog ofdialog;
        private object txtName;

        public ListController(MainWindow window)
        {
            Lwindow = window;
            sfdialog = new SaveFileDialog();
            ofdialog = new OpenFileDialog();
        }

        public void listEventHandler(object sender, RoutedEventArgs e)
        {
            Button B = (Button)sender;
            switch (B.Name)
            {
                case "AddButton":
                    ButtonAddName_Click();
                    break;
                case "SelectButton":
                    SelectFile();
                    break;
                case "ClearButton":
                    ClearDate();
                    break;
                case "SaveButton":
                    SaveDate();
                    break;
                
            }
        }

        private void ButtonAddName_Click()
        {
            if (!string.IsNullOrWhiteSpace(txtName.Text) && !List.Items.Contains(txtName.Text))
                lstNames.Items.Add(txtName.Text);

        }

        public void SelectFile()
        {

        }
        public void ClearDate()
        {
        

        }  
        public void SaveDate()
        {
            sfdialog.Filter = "Xml File (*.Xml)|*.xml";
            if (sfdialog.ShowDialog() == true)
            {
                Lista l;
                if (Lwindow.GetType().Equals(typeof(MainWindow)))
                {
                    l = ((MainWindow)Lwindow).GetData();
                }


                l.Toxml(sfdialog.FileName);
            }
        }
    }
}

